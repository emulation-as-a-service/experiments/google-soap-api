#!/usr/bin/env python

from spyne import Application, rpc, ServiceBase, Iterable, Integer, Unicode, Boolean, XmlAttribute, Double, Array

from spyne.protocol.soap import Soap11
from spyne.model.complex import ComplexModel
from spyne.server.wsgi import WsgiApplication
from spyne.model.primitive import String, Int

class DirectoryCategory:
    item = String

class DirectoryCategoryArray(ComplexModel):
    arrayType = XmlAttribute(Unicode, "optional");
    
class ResultElement(ComplexModel):
    summary = String
    URL = String
    snippet = String
    title = String
    cachedSize = String
    relatedInformationPresent = Boolean
    hostName = String
    directoryCategory = DirectoryCategory
    directoryTitle = String 

class ResultElementArray(ComplexModel):
    arrayType = XmlAttribute(Unicode, "optional");
    item = ResultElement.customize(max_occurs='unbounded')
    
class GoogleSearchResult(ComplexModel):
    documentFiltering = Boolean
    searchComments = String
    estimatedTotalResultsCount = Integer
    resultElements = ResultElementArray
    estimateIsExact = Boolean
    searchQuery = String
    startIndex = Integer
    endIndex = Integer
    searchTips = String
    directoryCategories = DirectoryCategoryArray
    searchTime = Double

class GoogleSearchService(ServiceBase):
    @rpc(Unicode, Unicode, Int, Int, Boolean, String, Boolean, String, String, String,
       _out_variable_name='return',
       _returns=GoogleSearchResult)
    def doGoogleSearch(ctx, 
	key, 
	q,
	start,
	maxResults,
        filter,
        restrict,
        safeSearch,
        lr,
        ie,
        oe):
      result = GoogleSearchResult();
      result.searchComments = "test"
      result.resultElements = ResultElementArray();
      result.resultElements.arrayType='ResultElement[3]'
      result.resultElements.item = [ResultElement(), ResultElement()]
      return result


application = Application([GoogleSearchService], 
			  tns='urn:GoogleSearch',
                          in_protocol=Soap11(validator='soft'),
                          out_protocol=Soap11())

wsgi_application = WsgiApplication(application)


if __name__ == '__main__':
    import logging

    from wsgiref.simple_server import make_server

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

    logging.info("listening to http://127.0.0.1:8000")
    logging.info("wsdl is at: http://localhost:8000/?wsdl")

    server = make_server('127.0.0.1', 8000, wsgi_application)
    server.serve_forever()

